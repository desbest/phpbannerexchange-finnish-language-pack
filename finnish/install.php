<?
$file_rev="041305";
$file_lang="en";
////////////////////////////////////////////////////////
//                 phpBannerExchange                  //
//                   by: Darkrose                     //
//              (darkrose@eschew.net)                 //
//                                                    //
// You can redistribute this software under the terms //
// of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of  //
// the License, or (at your option) any later         //
// version.                                           //
//                                                    //
// You should have received a copy of the GNU General //
// Public License along with this program; if not,    //
// write to the Free Software Foundation, Inc., 59    //
// Temple Place, Suite 330, Boston, MA 02111-1307 USA //
//                                                    //
//     Copyright 2004 by eschew.net Productions.      //
//   Please keep this copyright information intact.   //
////////////////////////////////////////////////////////

// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

//Install and upgrade will share some common variables..
$LANG_yes="Kyll�";
$LANG_no="Ei";
$LANG_title="phpBannerExchange asennusohjelma";
$LANG_install_verbage="<b>Tervetuloa phpBannerExchange 2.0 asennusohjelmaan!</b><p>T�m� ohjelma opastaa sinut l�pi phpBannerExchange 2.0n asennuksen tai p�ivityksen. T�m�n prosessin pit�isi olla melko kivuton ja sen pit�isi kest�� vain muutaman minuutin. T�ss� vaiheessa, sinun tulisi olla kohdassa <b>7</b> <a href=\"../docs/install.php#quickstart\">asennusoppaassa</a>. Ole Hyv� ja k�yt� t�t� opasta jos tulee mit� tahansa kysymyksi� eteen asennuksen aikana.";
$LANG_install_version_found="Olemme l�yt�neet ohjelman aikaisemman version! <a href=\"install.php?install=3&page=2\">Klikkaa t�st�</a> p�ivitt��ksesi.";
$LANG_install_version_donno="Emme l�yt�neet config.php tiedostosta versionumeroa, joten oletamme t�m�n olevan ensimm�inen asennus. Jos n�in ei ole, ole hyv� ja valitse asennustapa alapuolelta.";
$LANG_install_install="Uusi asennus";
$LANG_install_instdesc="Klikkaa t�t� linkki� jos et ole koskaan asentanut phpBannerExchange ohjelmaa serverillesi tai haluat suorittaa uuden puhtaan asennuksen. <b>VAROITUS:</b> T�m� tulee poistamaan kaikki tietokannassa mahdollisesti olevat phpBannerExchange taulut!";
$LANG_install_upgrade="P�ivit� 1.x:sta 2.0:aan";
$LANG_install_upgdesc="Valitse t�m� p�ivitt��ksesi phpBannerExchange versio 1.2 viimeisimp��n versioon.";

$LANG_install_rcupgrade="P�ivit� 2.0 RCx 2.0:ksi";
$LANG_install_rcupgdesc="K�yt� t�t� jos sinulla on vanhempi phpBannerExchange 2.0:n versio asennettuna (kuten 2.0 RC1), ja haluaisit p�ivitt�� viimeisimp��n versioon.";

$LANG_varedit_dirs="M��rit� j�rjestelm�n asetukset. N�m� ovat sinun globaalit asetuksesi jotka m��ritt�v�t asioita kuten bannereiden vaihtosuhteen, bannerivaihtosi nimen, adminin s�hk�postiosoitteen, jne. Yksityiskohdista l�yd�t tietoa katsomalla <a href=\"../docs/install.php\">asennusohjeet</a>.";

// headers
$LANG_varedit_dbhead="Tietokanta info";
$LANG_varedit_pathing="Polut & Admin tiedot";
$LANG_varedit_bannerhead="Bannerit";
$LANG_varedit_anticheathead="Anti-huijaus tiedot";
$LANG_varedit_refncredits="K�ytt�j�hankinnat ja pisteet";
$LANG_varedit_misc="Sekalaisia valintoja";

$LANG_varedit_dbhost="Tietokannan IP osoite (tai localhost)";
$LANG_varedit_dblogin="Tietokannan k�ytt�j�tunnus";
$LANG_varedit_dbpass="Tietokannan salasana";
$LANG_varedit_dbname="Tietokannan nimi";
$LANG_varedit_baseurl="Bannerivaihdon osoite (URL)";
$LANG_varedit_baseurl_note="�l� laita loppuun vinoviivaa";
$LANG_varedit_exchangename="Bannerivaihdon nimi";
$LANG_varedit_sitename="Sivuston nimi";
$LANG_varedit_adminname="Adminin nimi";
$LANG_varedit_adminemail="Adminin s�hk�postiosoite";
$LANG_varedit_width="Bannerin leveys";
$LANG_varedit_height="Bannerin korkeus";
$LANG_varedit_pixels="pixeli�";
$LANG_starting_credits="Aloituspisteet";
$LANG_varedit_imgpos="Bannerivaihdon kuvan sijoitus";
$LANG_varedit_duration="Kesto";
$LANG_varedit_duration_msg="Sekuntia..";
$LANG_varedit_showtext="N�yt� bannerivaihdon linkki";
$LANG_varedit_defrat="Oletussuhde";
$LANG_varedit_showimage="N�yt� bannerivaihdon kuva";
$LANG_varedit_imageurl="Bannerivaihdon kuvan osoite (URL)";
$LANG_varedit_imageurl_msg="koko osoite vaaditaan";
$LANG_varedit_sendemail="L�het� Adminille s�hk�postia";
$LANG_varedit_usepages="K�yt� sivujen numerointia";
$LANG_varedit_usemd5="K�yt� MD5 salakoodattuja salasanoja";
$LANG_varedit_topnum="Top x n�ytet��n";
$LANG_varedit_topnum_other="Tilit";
$LANG_varedit_upload="Salli tiedostojen l�hetys";
$LANG_varedit_maxsize="Suurin sallittu tiedostokoko";
$LANG_varedit_uploadpath="L�hetyspolku (ei vinoviivaa per��n)";
$LANG_varedit_upurl="L�hetyshakemiston osoite";
$LANG_varedit_referral="K�ytt�j�hankintaohjelma";
$LANG_varedit_bounty="K�ytt�j�hankintapalkkio";
$LANG_varedit_usegzhandler="K�yt� GZip pakkausta";
$LANG_varedit_usedbrand="K�yt� mySQL dbrand()";
$LANG_varedit_usedbrand_warn="VAIN mySQL 4+!";
$LANG_varedit_maxbanners="Suurin sallittu bannerim��r�";
$LANG_varedit_basepath="juuripolku";
$LANG_varedit_sellcredits="Myy pisteit�";
$LANG_varedit_anticheat="Anti-huijaus tapa";
$LANG_varedit_cookies="Ev�steet";
$LANG_varedit_db="Tietokanta";
$LANG_varedit_none="Ei mit��n";
$LANG_varedit_reqapproval="Vaadi bannerien hyv�ksymist�";
$LANG_varedit_usegz="K�yt� gZip/Zend koodia";
$LANG_varedit_userand="K�yt� mySQL4 rand()";
$LANG_varedit_userandwarn="Vaatii mySQL 4 tai uudemman";
$LANG_varedit_logclicks="Klikkaukset lokiin";
$LANG_left="Vasen";
$LANG_right="Oikea";
$LANG_top="Yl�s";
$LANG_bottom="Alas";
$LANG_varedit_reqbanapproval="Vaadi bannerien hyv�ksymist�";
$LANG_varedit_dateformat="P�iv�yksen muoto";

$LANG_varedit_submit="L�het�";
$LANG_varedit_reset="Tyhjenn�";

$LANG_fput_error_config="Kokoonpanotiedostoa ei voitu kirjoittaa. T�m� johtuu yleens� v��rist� oikeuksista kotisivujesi tarjoaja ei salli ohjelmien kirjoittaa tiedostoja. Tarkista onko tiedostolle annettu komento chmod 777. Ole Hyv� ja ota yhteys kotisivujesi tarjoajaan jos tarvitset avustusta t�ss� asiassa tai apua chmod komentojen antamiseen.";
$LANG_fput_chmod="Ohjelma yritti antaa chmod komentoa tiedostolle mutta ei onnistunut. T�m� voi johtua siit�, ett� tiedosto ei sijainnut oletetussa paikassa (tarkista base_path) asetus, tai j�rjestelm� ei ole kunnolla koottu k�sittelem��n chmod komentoja ohjelmista. Yrit� antaa chmod komento manuaalisesti (FTP tai SHELL yhteys). Ole Hyv� ja ota yhteys kotisivujesi tarjoajaan jos tarvitset avustusta t�ss� asiassa tai apua chmod komentojen antamiseen.";
$LANG_fput_success="Kokoonpanotiedosto on kirjoitettu onnistuneesti! Asennusohjelma on nyt valmis siirtym��n asennuksen seuraavaan vaiheeseen.";
$LANG_db_problem="Yhteydenotossa kokoonpanotiedostossa m��rittelem��si tietokantaan oli ongelma. Ole Hyv� ja tarkista tietokantasi ja varmista ett� <b>$dbname</b> niminen tietokanta on olemassa.";
$LANG_db_noconnect="Yhteydenotossa tietokantaan oli ongelma! Ole Hyv� ja varmista ett� sinulla on tarvittavat oikeudet k�ytt�� kokoonpanotiedostossa m��rittelem��si tietokantaa ja ett� salasana on oikein.";

$LANG_tables_created="Kaikki taulut luotu onnistuneesti!";
$LANG_upgrade_db="P�ivit�n tauluja, ole hyv� ja odota.";
$LANG_upgrade_done="Taulut p�ivitetty!";
$LANG_admin_add_instructions="Luo admin tilisi. phpBannerExchange tukee useita k�ytt�j�tunnuksia per k�ytt�j�, lis�� k�ytt�j�tunnuksia voi luoda kirjautumalla Adminin hallintapaneeliin asennuksen j�lkeen.";
$LANG_admin_login="Adminin kirjautuminen";
$LANG_admin_pass="Salasana";
$LANG_again="Uudelleen";
$LANG_password_mismatch="Salasanat jotka annoit eiv�t t�sm��. Ole Hyv� ja paina Takaisin nappulaa ja yrit� uudelleen.";

$LANG_install_complete="phpBannerExchange 2.0:n asennus on valmis! Voit nyt kirjautua <a href=\"../admin/\">Adminin hallintapaneeliin</a> admin k�ytt�j�tunnuksella ja salasanalla.<p><b>T�RKE��! VARMISTA ETT� POISTAT KOKO INSTALL HAKEMISTON SERVERILT�SI!</b>";

$LANG_continue="Jatka";

$LANG_install_oldvarupgrade="<b>Huomaa</b>: Sinun <b>T�YTYY</b> k�ytt�� eri nimist� tietokantaa jos olet p�ivitt�m�ss� phpBannerExchange versiota 1.x v�ltt��ksesi tiedon h�vi�misen! Asennusohjelma voi tuoda vanhat tilitiedot tietokannasta, MUTTA sen t�ytyy olla eri tietokanta kuin mit� k�ytet��n p�ivitetyn version kanssa!";
$LANG_install_oldverupg="Asennusohjelma on nyt valmis p�ivitt�m��n vanhat tilisi. T�m� voi vied� jonkin aikaa riippuen siit� kuinka paljon tilej� vanhassa tietokannassa on. Jatkaaksesi, anna alapuolella vanhan tietokantasi nimi:";
?>