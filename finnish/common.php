<?
$file_rev="041305";
$file_lang="en";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Menu items..
$LANG_menu_options="Vaihtoehdot";
$LANG_backtologin="Kirjautuminen";
$LANG_lostpw="Salasana hukassa?";
$LANG_faq="UKK";
$LANG_signup="Rekister�idy";
$LANG_rules="Bannerivaihdon s��nn�t";
$LANG_tocou="K�ytt�ehdot";

$LANG_menu_extras="Muuta";
$LANG_topbanns="Top Bannerit";
$LANG_overallstats="Yleistilastot";

// login page (/index.php)
$LANG_indtitle="Kirjaudu hallintapaneeliin";
$LANG_headertitle="Bannerivaihdon hallintapaneeli";
$LANG_login_instructions="Anna alapuolella k�ytt�j�tunnuksesi ja salasanasi. Jos haluat liity� bannerivaihtoon, <a href=\"cou.php\">Klikkaa t�st�</a> luodaksesi uuden tilin!";
$LANG_login="K�ytt�j�nimi:";
$LANG_pw="Salasana:";
$LANG_login_button="Kirjaudu";

//Recover Password (/recoverpw.php)
$LANG_lostpw_title="Salasanan palautus";
$LANG_lostpw_instructions="Saadaksesi uuden salasanan, anna s�hk�postiosoite jota k�ytit rekister�ityess�si bannerivaihtoon. Uusi salasana luodaan ja l�hetet��n s�hk�postitse tuohon osoitteeseen.";
$LANG_lostpw_instructions2=" Turvallisuussyist� ei ole mahdollista l�hett�� automaattisesti luotuja uusia salasanoja eri s�hk�postiosoitteeseen.";
$LANG_lostpw_recover="Salasanasi on vaihdettu ja saat pian uuden salasanasi s�hk�postin mukana.";
$LANG_lostpw_email="S�hk�postiosoite";
$LANG_lostpw_success="Salasanasi on vaihdettu! Saat pian uuden salasanasi s�hk�postin mukana. Kun olet kirjautunut sis��n, voit vaihtaa salasanasi bannerivaihdon hallintapaneelissa.";

// Signup Form (/signup.php)
$LANG_signupwords="Rekister�idy $exchangename :on";
$LANG_realname="Oikea nimi";
$LANG_pw_again="Salasana uudelleen";
$LANG_cat="Kategoria";
$LANG_catstuff="Ole Hyv� ja valitse kategoria";
$LANG_email="S�hk�postiosoite";
$LANG_siteurl="Sivuston osoite (URL)";
$LANG_bannerurl="Bannerin osoite (URL)";
$LANG_signsub="Paina kerran l�hett��ksesi";
$LANG_signres="Tyhjenn�";
$LANG_newsletter="L�het� uutiskirje";
$LANG_coupon="Kupongin koodi";
$LANG_rejected="Emme voineet luoda tili�si seuraavista syist�:";

// success messages..
$LANG_signup_thanks="Kiitos rekister�itymisest�!";
$LANG_signupinfo="Tilisi on lis�tty onnistuneesti! Sinut tullaan lis��m��n bannerien kiertoon parin p�iv�n sis�ll�. Olemme my�s l�hett�neet s�hk�postia osoitteeseen $email sis�lt�en k�ytt�j�tunnuksesi ja salasanasi. Pid� t�m� tallessa jos satut my�hemmin tarvitsemaan.  Voit kirjautua my�s menem�ll� <a href=\"index.php\">Tilastoihinkirjautumissivulle</a> milloin tahansa.  T�m� paneeli antaa sinulle mahdollisuuden lis�t� bannereita, tarkistaa tilastosi, vaihtaa osoitettasi (URL), vaihtaa salasanaasi, jne.<p>";
$LANG_coupon_added="Tili�si on hyvitetty <b>$newcredits</b> pisteell� k�ytetty�si kuponkia.";
$LANG_signup_uploadmsg="Huomaa: Sinun tulee kirjautua sis��n ja l�hett�� banneri jotta tilisi voidaan hyv�ksy�!";

// Conditions of Use page (/conditions.php)
$LANG_coutitle="K�ytt�ehdot";
$LANG_header="J�sensopimus";
$LANG_agree="Hyv�ksyn";
$LANG_disagree="En hyv�ksy";

// Top banners/accounts page (/top.php)
//top 10 banners:
$LANG_top10_title="Top $topnum accounts";
$LANG_top10_exposure="N�ytt�kerrat";
$LANG_top10_banners="Banneri";
$LANG_top10_nobanners="Bannerivaihdossa ei ole viel� hyv�ksyttyj� bannereita, joten emme voi n�ytt�� mit��n tietoa! Huomaa: Oletusbannereita EI n�ytet� t�ll� sivulla, vain normaalit k�ytt�jien tilit n�ytet��n!";

//Overall stats (/overall.php)
$LANG_overall_totusers="K�ytt�j�t";
$LANG_overall_exposures="N�ytt�kerrat";
$LANG_overall_banners="Bannerit";
$LANG_overall_totclicks="Klikkaukset sivustoille";
$LANG_overall_totsiteclicks="Klikkaukset sivustoilta";
$LANG_overall_ratio="Klikkaussuhde";


// common stuff...
$LANG_yes="Kyll�";
$LANG_no="Ei";
$LANG_top="Yl�s";
$LANG_topics="Aiheet";

//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!
?>