<?
$file_rev="041305";
$file_lang="en";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

//Common messages
//menu:
$LANG_menu_nav="Navigointi";
$LANG_menu_home="Etusivu";
$LANG_menu_logout="Kirjaudu ulos";

$LANG_menu_stats="Tilastot";
$LANG_menu_emstats="Tilastot s�hk�postitse";

$LANG_menu_site="Sivustosi";
$LANG_menu_commerce="Osta pisteit�";
$LANG_menu_banners="Bannerisi";
$LANG_menu_cat="Vaihda kategoriaa";
$LANG_menu_htmlcode="Hae HTML koodi";

$LANG_menu_info="Tietosi";
$LANG_menu_changeem="Vaihda s�hk�postiosoitetta";
$LANG_menu_changepass="Vaihda salasanaa";
$LANG_coupon_menuitem="Kirjaa tarjouskoodi";

//Common stuff
$LANG_reset="Tyhjenn�";

// Stats Page (client/stats.php)
$LANG_stats_title="Hallintapaneeli - ";
  //Stats Window stuff
$LANG_stats_startdate="Aloitusp�iv�ys";
$LANG_stats_siteexpos="Bannerit jotka n�ytetty sivustollasi";
$LANG_stats_siteclicks="Klikkaukset sivustoltasi";
$LANG_stats_percent="Prosentit";
$LANG_stats_ratio="Suhde";
$LANG_stats_exposures="N�ytt�kerrat";
$LANG_stats_avgexp="Keskim��r�iset n�ytt�kerrat/vrk";
$LANG_stats_clicks="Klikkaukset sivustollesi";
$LANG_commerce_credits="Pisteet";
  // Explanation of stats Window Stuff. These next 2 groups
  // are separate even though they say roughly the same thing
  // to keep them easier to manage.
  // 
  // This first one is for a normal x:1 ratio site..Full message
  // reads: "To date, you have displayed [x] banners on your site,
  // and generated [y] clicks from those exposures. You have earned
  // [z] credits for your own banner on other sites (you earn [n]
  // exposure(s) for each banner you display)."
$LANG_stats_exp_normal="<br>T�h�n menness�, olet n�ytt�nyt";
$LANG_stats_exp_normal1="banneria sivustollasi, ja tuottanut";
$LANG_stats_exp_normal2="klikkausta noista n�ytt�kerroista.  Olet ansainnut";
$LANG_stats_exp_normal3="pistett� omasta banneristasi muiden sivustoilla (Ansitset"; 
$LANG_stats_exp_normal4="n�ytt�kertaa jokaisesta bannerista jonka n�yt�t).";
  // This one is for "odd" ratio sites like 5:4 ..Full message
  // reads: "To date, you have displayed [x] banners on your site,
  // and generated [y] clicks from those exposures. You have earned
  // [z] credits for your own banner on other sites (you earn [n]
  // exposure(s) for each [n] displays)."
$LANG_stats_exp_weird="<br>T�h�n menness�, olet n�ytt�nyt";
$LANG_stats_exp_weird1="banneria sivustollasi, ja tuottanut";
$LANG_stats_exp_weird2="klikkausta noista n�ytt�kerroista.  Olet ansainnut";
$LANG_stats_exp_weird3="pistett� omasta banneristasi muiden sivustoilla.  (Ansaitset"; 
$LANG_stats_exp_weird4="n�ytt�kertaa jokaisesta <b>$banexp</b> n�yt�st�.)";
  // Approved or not approoved messages.
$LANG_stats_approved="Tilisi on hyv�ksytty ja on vaihtokierrossa.";
$LANG_stats_unapproved="Tilisi odottaa adminin hyv�ksynt��.  Kun se on hyv�ksytty, se lis�t��n kiertoon, ja kaikki ansaitut pisteet muutetaan n�ytt�kerroiksi.";
$LANG_stats_bannercount="Kaikki Bannerit:";
 // Referral messages. Reads: "You have earned [x] credits by
 // referring [y] account(s) to the exchange. Currently, there
 // are [z] accounts referred by you awaiting validation..(etc)".
$LANG_stats_referral1="Olet ansainnut";
$LANG_stats_referral2="pistett� hankkimalla";
$LANG_stats_referral3="k�ytt�j�tili� bannerivaihtoon. T�ll� hetkell� on";
$LANG_stats_referral4="tili� jotka olet hankkinut odottamassa vahvistusta. K�ytt�j�hankintapisteet maksetaan vasta kun admin on tilit hyv�ksynyt.";
  // Tips
$LANG_tip_startdate="P�iv�ys jolloin tilisi on luotu.";
$LANG_tip_siteexposure="Bannereiden m��r� jotka on n�ytetty sivustollasi.";
$LANG_tip_clickfrom="Klikkausten lukum��r� jonka ovat tuottaneet bannerit sinun sivustollasi.";
$LANG_tip_percentout="Prosentuaalinen m��r� vierailijoista sivustollasi jotka klikkaavat banneria toiselle sivustolle.";
$LANG_tip_ratioout="Suhde joka muodostuu sivustosi vierailijoista jotka klikkaavat banneria toiselle sivustolle.";
$LANG_tip_exposures="Kuinka monta kertaa bannerisi on n�kynyt toisilla sivustoilla.";
$LANG_tip_avgexp="Keskim��r�inen bannereidesi n�ytt�kertojen m��r� per vuorokausi muilla sivustoilla.";
$LANG_tip_clicks="Kuinka monta kertaa banneriasi on klikattu kun se on n�kynyt muilla sivustoilla";
$LANG_tip_percentin="Prosentuaalinen m��r� vierailijoista jotka klikkasivat banneriasi muilla sivustoilla.";
$LANG_tip_ratioin="Suhde joka muodostuu vierailijoista jotka klikkasivat banneriasi muilla sivustoilla.";
$LANG_tip_credits="K�ytt�m�tt�mien pisteiden m��r� jotka tilisi on ker�nnyt n�ytt�m�ll� bannereita sivustollasi.";

// Log Out page (/client/logout.php)
$LANG_logout_title="Kirjautunut ulos";
$LANG_logout_message="Olet onnistuneesti kirjautunut ulos!<p><a href=\"../index.php\">Klikkaa T�st�</a> palataksesi kirjautumisn�ytt��n.";

// Email Stats (/client/emailstats.php)
$LANG_emailstats_title="L�het� tilastot s�hk�postilla";
$LANG_emailstats_msg="Olemme l�hett�neet tilisi tilastot s�hk�postitse osoitteeseen $email . Sinun tulisi saada se pian.";

// Commerce/Buy Credits (/client/commerce.php)
$LANG_commerce_noitems="T�ll� hetkell� pisteit� ei voi ostaa";
$LANG_commerce_name="Tuotteen nimi";
$LANG_commerce_price="Hinta";
  // "Buy now via [service]". In the future, phpBannerExchange
  // will support multiple payment services.
$LANG_commerce_buynow_button="Osta nyt";
$LANG_commerce_buynow="Osta nyt";
$LANG_commerce_history="Ostoshistoriasi";
$LANG_commerce_date="P�iv�ys";
$LANG_commerce_item="Tuote";
$LANG_commerce_purchaseprice="Myyntihinta";
$LANG_commerce_invoice="Lasku";
$LANG_commerce_nohist="Ei ostohistoriaa!";
$LANG_commerce_couponhead="Kuponki";
$LANG_commerce_coupon_button="K�yt� kuponkia";

// Banners (/client/banners.php)
$LANG_targeturl="Kohde-osoite (URL)";
$LANG_filename="Tiedostonimi";
$LANG_views="N�yt�t";
$LANG_clicks="Klikkaukset";
$LANG_bannerurl="Bannerin osoite (URL)";
$LANG_menu_target="Vaihda osoitetta (URL)";
$LANG_button_banner_del="Poista banneri";
$LANG_stats_hdr_add="Lis�� banneri";
$LANG_banner_instructions="Muokataksesi bannerin kohde-osoitetta (URL) tai bannerin osoitetta (URL), muuta tietoja oikeassa kohdassa, sitten klikkaa <b>Vaihda osoitetta(s)</b> nappia. Poistaaksesi bannerin, klikkaa <b>Poista banneri</b> linkki�. K�yd�ksesi bannerin kohde-osoitteessa m��ritetyll� sivustolla, klikkaa banneria joka kuuluu tuohon kohde-osoitteeseen.";

// this variable displays at the bottom of the "Banners" page.
// eg: "4 banner(s) found for your account"
$LANG_banner_found="tilisi banneri(a) l�ydetty";
$LANG_stats_nobanner="tilisi bannereita ei l�ydetty!";

// Delete Banner (/client/deletebanner.php and /client/deleteconfirm.php)
$LANG_delban_title="Poista banneri";
$LANG_delban_warn="Oletko varma ett� haluat poistaa t�m�n bannerin? T�t� toimintoa ei voi peruuttaa.<br>";
$LANG_delban_button="Kyll�, Poista t�m� banneri";
$LANG_delbanconf_verbage="Banneri on poistettu!";
$LANG_delbanconf_success="Banneri on poistettu onnistuneesti tililt�si";

// Category page (/client/category.php and /client/categoryconfirm.php)
$LANG_cat_reval_warn="Kategorian muuttaminen vaatii tilisi uudelleenhyv�ksynt�� adminilta. (ansaitset silti yh� pisteit� sen aikaa kun tilisi odottaa hyv�ksynt��).";
$LANG_cat_change_button="Muuta kategoria";
$LANG_cat_nocats="Admin ei ole m��ritt�nyt yht��n kategoriaa, joten et voi muuttaa kategoriaa t�ll� hetkell�.";
$LANG_catconf_message="Kategoriasi on muutettu!";

// Get HTML (/client/gethtml.php)
$LANG_gethtml_title="Hae HTML";
$LANG_gethtml_message="Jos haluat k�ytt�� kategoria-ominaisuutta n�ytt��ksesi enemm�n kohdistettuja bannereita sivustollasi, korvaa \"&cat=0\" sopivalla kategrian numerolla alla olevasta taulukosta (esimerkki: \"&cat=2\", \"&cat=3\", ja niin edelleen). Varmista ett� k�yt�t yl�puolella samaa numeroa molempiin koodeihin. T�m� varmentaa sen ett� saat oikeat pisteet sivujesi n�yt�ist�. Jos j�t�t t�m�n arvon niin kuin se on nyt, kaikkien tilien bannerit n�kyv�t, huolimatta valitusta kategoriasta.";
$LANG_gethtml_catname="Kategorian nimi";
$LANG_gethtml_catid="Kategorian tunnus";

// Change email address (/client/editinfo.php)
$LANG_email_title="Muokkaa tili�si";
$LANG_email_address="S�hk�postiosoite";
$LANG_email_button="Vaihda s�hk�postiosoitetta";

// Change email confirmation (/client/editconfirm.php)
 $LANG_infoconfirm_title="Muokkaa tili�si";
 $LANG_infoconfirm_success="Olemme muuttaneet s�hk�postiosoitteeksesi ";

// Change PW form (/client/editpass.php)
$LANG_pass_title="Vaihda salasanasi";
$LANG_pass1_label="Uusi salasana";
$LANG_pass2_label="Uudelleen";
$LANG_pass_button="Vaihda salasana";
$LANG_pass_confirm="Salasanasi on vaihdettu! Sinun pit�� nyt <a href=\"logout.php\">kirjautua ulos</a> ja takaisin sis��n uudella salasanalla.";

// Buy Credits page (/client/promo.php)
$LANG_coupon_menuitem="Anna tarjouskoodi";
$LANG_coupon_instructions="Anna alapuolelle tarjouskoodi jonka sait adminilta.";
$LANG_submit="L�het�";
$LANG_coupon_success="<b>Kuponki on lunastettu!</b>";
$LANG_coupon_success2="$credits pistett� on lis�tty tilillesi ja on k�ytett�viss� v�litt�m�sti.";

// Click Log (/client/clicklog.php)
$LANG_clicklog="Klikkaa lokia";
$LANG_clicklog_from="Sivustoltasi";
$LANG_clicklog_to="Sivustollesi";
$LANG_clicklog_ip="IP osoite";
$LANG_clicklog_date="P�iv�ys/Kellonaika";
$LANG_noclicks="Ei klikkauksia n�ytett�v�ksi.";


//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!
?>