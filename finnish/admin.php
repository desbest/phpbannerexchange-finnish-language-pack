<?
$file_rev="041305";
$file_lang="EN";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// admin menu
// titles
$LANG_menu_acct="Tilit";
$LANG_menu_administration="Hallinto";
$LANG_menu_tools="Ty�kalut";
$LANG_menu_nav="Navigointi";

// options..
// accounts
$LANG_menu_valacct="Vahvista";
$LANG_menu_addacct="Lis�� tilej�";
$LANG_menu_listacct="Listaa tilit";
$LANG_menu_changedefault="Oletus banneri";

// administration
$LANG_menu_mailer="S�hk�postin hallinta";
$LANG_menu_categories="Kategorioiden hallinta";
$LANG_menu_editpass="Vaihda tunnussana";
$LANG_menu_addadmin="Lis��/Poista Admin";

// tools
$LANG_menu_dbtools="Tietokannan ty�kalut";
$LANG_menu_templates="Muokkaa sivun mallia";
$LANG_editvars_title="Muokkaa asetuksia";
$LANG_menu_editcss="Muokkaa tyyli�";
$LANG_menu_faqmgr="UKK hallinta";
$LANG_menu_checkbanners="Tarkista bannerit";
$LANG_menu_editcou="Muokkaa k�ytt�ehtoja";
$LANG_menu_editrules="Muokkaa s��nt�j�";
$LANG_promo_title="Tarjouksien hallinta";
$LANG_menu_pause="Keskeyt� toiminta";
$LANG_menu_unpause="Jatka toimintaa";
$LANG_commerce_title="Kaupan hallinta";
$LANG_updatemgr_title="P�ivityksien hallinta";

// navigation
$LANG_menu_help="Ohjeet";
$LANG_menu_home="Etusivu";
$LANG_menu_logout="Kirjaudu ulos";

//Stats Page (/admin/stats.php)
$LANG_stats_nopend="Vahvistamattomia tilej� ei ole.";
$LANG_stats_pend_sing="Varmentamaton tili l�ydetty.";
$LANG_stats_pend_plur="Varmentamattomia tilej� l�ydetty.";
$LANG_stats_title="Admin hallintapaneeli";
$LANG_stats_statssnapshot="Tilannevedos: Tilastot -";
$LANG_stats_valusr="Varmennetut k�ytt�j�t";
$LANG_stats_pendusr="Varmentamattomat k�ytt�j�t";
$LANG_stats_totexp="Kaikki n�ytt�kerrat";
$LANG_stats_loosecred="Irralliset pisteet";
$LANG_stats_totalban="Kaikki n�ytetyt bannerit";
$LANG_stats_totclicks="Kaikki klikkaukset sivustoille";
$LANG_stats_totsicl="Kaikki klikkaukset sivustoilta";
$LANG_stats_overrat="Koko vaihtosuhde";
$LANG_stats_pendacct="Varmentamattomat tilit";
$LANG_stats_addacct="Lis�� tili";

// Paused message for Stats page
$LANG_exchange_paused="<b>TOIMINTA ON PYS�YTETTY!</b> T�m� tarkoittaa ett� vain oletusbannerit n�ytet��n. K�ytt�j�t saavat edelleen pisteit� n�ytt�kerroista. Palataksesi takaisin normaaliin toimintaan, klikkaa <b>Jatka toimintaa</b> linkki�.";

// Validate account (/admin/validate.php)
$LANG_val_instructions="Seuraavat tilit odottavat varmentamista. Varmentaaksesi tilin, klikkaa tilin nime�.";
// $LANG_val_awaiting is for the number of accounts at the bottom
// of the validation page. (eg: "3 account(s) awaiting validation")
$LANG_val_awaiting="Tili(t) jotka odottavat varmentamista.";
$LANG_val_noaccts="T�ll� hetkell� ei ole yht��n varmentamatonta tili�.";

// Add Account (/admin/addacct.php)
// uses some of the edit account form/lang files
$LANG_addacct_title="Lis�� tili";
$LANG_addacct_msg="Tili on lis�tty!";
$LANG_addacct_button="Etusivu";

// The following are shared between the Add Account,
// edit account and validate account pages.
$LANG_edit_realname="Oikea nimi";
$LANG_edit_login="K�ytt�j�tunnus";
$LANG_edit_pass="Salasana";
$LANG_edit_email="S�hk�postiosoite";
$LANG_edit_category="Kategoria";
$LANG_edit_nocats="Admin ei ole viel� m��ritellyt yht��n kategoriaa, joten et voi t�ll� hetkell� vaihtaa kategoriaa.";
$LANG_edit_exposures="N�ytt�kerrat";
$LANG_edit_credits="Pisteet";
$LANG_edit_clicks="Klikkaukset sivustolle";
$LANG_edit_siteclicks="Klikkaukset sivustolta";
$LANG_edit_raw="RAW moodi";
$LANG_edit_status="Tilin tila";
$LANG_edit_approved="Hyv�ksytty";
$LANG_edit_notapproved="Ei hyv�ksytty";
$LANG_edit_defaultacct="Oletustili";
$LANG_edit_sendletter="L�het� Uutiskirje";
$LANG_edit_button_val="Varmenna tili";
$LANG_edit_button_reset="Tyhjenn� t�m� sivu";
$LANG_edit_button_delraw="Posta RAW HTML";
$LANG_edit_button_del="Poista tili";

// Edit Account (/admin/edit.php)
$LANG_edit_title="Muokkaa tili�";
$LANG_edit_heading="Muokkaa/Varmenna tili";
// send e-mail link to the right of the e-mail field
$LANG_email_button_send="L�het� s�hk�postia";
$LANG_edit_saleshist="Pisteiden myyntihistoria";
// for viewing raw mode HTML
$LANG_edit_raw_current="t�m�nhetkinen";
$LANG_edit_button_addban="Lis�� banneri";
$LANG_edit_button="Takaisin tilastoihin";
$LANG_edit_bannerlink="Katso/Muokkaa bannereita";
$LANG_stats_banner_hdr="Bannerit";
$LANG_stats_hdr_add="Lis�� banneri";
// Banner report at the bottom of the edit page
// eg: "3 active banners found for [accountname]"
$LANG_edit_banners="Toimivia bannereita l�ydetty -";

// Edit/validate Confirm message:
$LANG_editconf_msg="Tili� on muokattu.";
$LANG_valconf_msg="Tili on varmennettu.";

// Banners (/admin/banners.php)
$LANG_targeturl="Kohteen osoite";
$LANG_filename="Tiedostonimi";
$LANG_views="Katsomiskertoja";
$LANG_clicks="Klikkauksia";
$LANG_bannerurl="Bannerin osoite";
$LANG_menu_target="Vaihda osoite/osoitteet";
$LANG_button_banner_del="Poista banneri";
$LANG_banner_instructions="Muokataksesi bannerin kohde-osoitetta tai bannerin osoitetta, muokkaa tietoja oikeassa kohdassa, sitten klikkaa <b>Muokkaa osoitteita</b> nappia. Poistaaksesi bannerin, klikkaa <b>Poista banneri</b> linkki�. K�yd�ksesi sivustolla joka on m��ritelty kohteen osoitteessa, klikkaa banneria joka kuuluu kyseisen kohteen osoitteeseen.";

// this variable displays at the bottom of the "Banners" page.
// eg: "4 banner(s) found for this account"
$LANG_banner_found="banneri(a) l�ydetty t�lle tilille";
$LANG_stats_nobanner="bannereita ei l�ydetty t�lle tilille!";

// Delete Banner (/admin/deletebanner.php)
$LANG_delban_title="Poista banneri";
$LANG_delban_verbage="Oletko varma ett� haluat poistaa t�m�n bannerin? T�m� toiminto on peruuttamaton.<br>";
$LANG_delban_go="Kyll�, Poista t�m� banneri";
$LANG_delbanconf_verbage="Banneri on poistettu!";

// Account Listing page (/admin/listall.php)
$LANG_listall_title="Luettele kaikki tilit";
$LANG_listall_button_back="Takaisin tilastoihin";
$LANG_listall_default="Oletustilit";
$LANG_listall_nodef="Oletustilej� ei l�ydetty.";
$LANG_listall_def_sing="Oletustili l�ydetty.";
$LANG_listall_def_plur="Oletustilej� l�ydetty.";
$LANG_listall_nonorm="Normaaleja tilej� ei l�ydetty.";
$LANG_listall_norm_head="Normaalit toimivat tilit";
$LANG_listall_norm_sing="normaali tili l�ydetty.";
$LANG_listall_norm_plur="normaalia tili� l�ydetty.";

// Delete Account Page (/admin/deleteaccount.php)
$LANG_delacct_title="Poista tili";
$LANG_delacct_verbage="Oletko varma ett� haluat pysyv�sti poistaa tilin jonka k�ytt�j�tunnus on";
$LANG_delacct_go="Kyll�, poista tili";
$LANG_delacct_done="Seuraava tili on poistettu:";

// Default Banner (/admin/changedefaultbanner.php)
$LANG_menu_changedefault="Oletusbanneri";
$LANG_changedefault_title="Vaifda oletusbanneri";
$LANG_changedefault_message="T�m� toiminto vaihtaa oletusbannerin jos yksik��n banneri ei ole kelvollinen esitett�v�ksi. T�m� ei ole sama kuin oletustili, koska oletustili ei ole laskuri-tili. Oletusbanneri t�ll� hetkell�:";
$LANG_changedefault_url="Kohde";
$LANG_changedefault_nodefault="Oletusbanneria ei ole t�ll� hetkell�!";
$LANG_changedefault_bannerurl="Bannerin osoite";

// Email All Accounts (/admin/email.php)
$LANG_email_title="S�hk�posti kaikille tileille";
$LANG_email_override="Kumoa k�ytt�j�n asetukset";
$LANG_email_override_warning="K�yt� t�t� optiota *S��STELI��STI*";
$LANG_email_message="Viesti:<p>Katso ohje-tiedostosta tietoa n�iden muuttujien toiminnoista. Kelpaavia muuttujia ovat";
$LANG_email_button_reset="Tyhjenn� t�m� sivu";
$LANG_email_address="S�hk�postiosoite";
$LANG_email_user="S�hk�postia k�ytt�j�lle";
$LANG_email_senttouser="S�hk�posti on l�hetetty k�ytt�j�lle:";
$LANG_email_return="klikkaa t�st�</a> palataksesi tilinmuokkausn�ytt��n.";
$LANG_email_allcats="Kaikki kategoriat";

// Email Sending status page (/admin/emailgo.php)
$LANG_emailgo_title="S�hk�postia kaikille tileille";
$LANG_emailgo_msg_all="S�hk�postin l�hetys <b>kaikille</b> tileille (sis�lt�en my�s ne jotka eiv�t ole s�hk�postilistalla)...t�m� voi vied� jonkin aikaa.";
$LANG_emailgo_msg_only="S�hk�postin l�hetys tileille jotka haluavat uutiskirjeet...t�m� voi vied� jonkin aikaa.";

// Email confirmation page (/admin/emailsend.php)
$LANG_emailconf_title="S�hk�postia kaikille tileille - VAHVISTA";
$LANG_emailconf_subject="Aihe";
$LANG_emailconf_msg="Viesti";
$LANG_emailconf_button_send="L�het� s�hk�posti";
$LANG_emailconf_button_reset="Tyhjenn� t�m� sivu";

// Category Admin (/admin/catmain.php)
$LANG_catmain_title="Kategorioiden hallinta";
$LANG_catmain_header="Nykyiset kategoriat";
$LANG_catmain_catname="Kategorian nimi";
$LANG_catmain_sites="Sivustot";
$LANG_catmain_addcat="Lis�� kategoria";
$LANG_catsfound_singular=" kategoria l�ydetty.";
$LANG_catsfound_plurl=" kategoriaa l�ydetty.";

// Delete Category (/admin/delcat.php)
$LANG_delcat_title="Poista kategoria";
$LANG_delcat_acctexist="T�ss� kategoriassa on t�ll� hetkell� <b>$get_count</b> tili�. T�m�n kategorian poistaminen siirt�� n�m� tilit oletuskategoriaan.  Oletko <b>*VARMA*</b> ,ett� haluat tehd� t�m�n?";
$LANG_delcat_sure="Oletko varma, ett� haluat poistaa t�m�n kategorian?";
$LANG_delcat_button="Kyll�, poista kategoria";

// Delete Category Confirmation (/admin/delcatconf.php)
$LANG_delcatconf_reset="P�ivit�n kaikki tilit poistetussa kategoriassa oletuskategoriaan (t�m� voi vied� aikaa, <b>$get_num</b> sivustoa t�ytyy siirt��)";
$LANG_delcatconf_status="Vaihdan kategoriaa <b>$name</b> tilille (tunnus: <b>$id</b>)";
$LANG_delcatconf_success="Kategoria on poistettu";

// Edit Category (/admin/editcat.php)
$LANG_editcat_title="Muokkaa kategoriaa";
$LANG_editcat_catname="Kategorian nimi";
$LANG_editcat_success="Kategoriaa on muokattu!";

// Add Admin page (/admin/addadmin.php)
$LANG_addadmin_title="Lis��/Poista Admin tili";
$LANG_addadmin_list="Nykyiset Adminit";
$LANG_addadmin_newlogin="Uusi kirjautuminen";
$LANG_addadmin_pass1="Salasana";
$LANG_addadmin_pass2="Salasana uudelleen";

// Change Password page (/admin/editpass.php)
$LANG_editpass_title="Vaihda Adminin salasana";
$LANG_editpass_newpass="Uusi salasana";
$LANG_editpass_newpass1="Uusi salasana uudelleen";
$LANG_editpass_button="Vaihda salasana";
$LANG_editpass_reset="Tyhjenn� t�m� sivu";

//Password confirmation page (/admin/pwconfirm.php)
$LANG_pwconfirm_title="Vaihda salasana";
$LANG_pwconfirm_success="Salasanasi on vaihdettu! Ole hyv�, ja <a href=\"index.php\">palaa kirjautumissivulle</a> ja kirjaudu uudestaan uudella salasanallasi.";

// Database tools.. (/admin/dbtools.php)
$LANG_db_title="Tietokanta-ty�kalut";
$LANG_db_buname="Varmennus tehty";
$LANG_db_budate="P�iv�ys luotu";
$LANG_db_delete="Poista";
$LANG_db_backupfiles="Varmenna tiedostot";
$LANG_db_newbuset="Luo uusi varmennus";
$LANG_db_instructions="Luodaksesi uuden varmennuksen, klikkaa \"Luo uusi varmennus\" linkki� (Voi vied� aikaa jos tietokanta on suuri). Klikkaa tiedoston nime� kun varmennus on luotu katsellaksesi sit� selaimessa. Klikkaa Tiedosto/Tallenna selaimessasi tallentaaksesi tiedoston koneellesi.<p><b>VAROITUS!</b> Koska varmennushakemisto on kaikkien luettavissa ja kirjoitettavissa, on suositeltavaa, ett� luot .htaccess tiedoston salasanasuojataksesi hakemiston! V�hint��n, varmista, ett� varmennustiedostot on poistettu sen j�lkeen kun olet ladannut ne koneellesi/katsonut ne.";
$LANG_db_restore="Palauta";
$LANG_db_upload="L�het� varmennus";
$LANG_db_upload_button="L�het�";

// Edit Templates (/admin/templates.php)
$LANG_menu_templates="Muokkaa sivumallia";
$LANG_templates_message="Valitse sivumalli jota haluaisit muokata alapuolella olevasta alasveto-valikosta. T�m�n j�lkeen sivu latautuu tekstilaatikossa. Kun olet tyytyv�inen muutoksiisi, klikkaa L�het� nappia tallentaaksesi mallin.";
$LANG_templates_warning="VAROITUS: T�m� todella muuttaa mallejasi ja voit luoda itsellesi ongelmia muuttamalla niit� jos et tied� mit� olet tekem�ss�! Muuta malleja omalla vastuulla!";
$LANG_template_box="Sivumalli";
$LANG_template_choose="Ole Hyv� ja valitse malli..";
$LANG_preview="Esikatselu";
$LANG_valid_tags="Kelpaavat tagit ovat:";

// Edit variables page (/admin/editvars.php)
$LANG_editvars_title="Muokkaa asetuksia";
$LANG_varedit_dirs="M��rittele j�rjestelm�n asetukset. N�m� ovat sinun globaaliset muuttujasi jotka m��ritt�v�t asioita kuten bannerien vaihtosuhteen, bannerivaihtosi nimen, adminin s�hk�postiosoitteen, jne. Katso <a href=\"../docs/install.php\">asennusohjeista</a> yksityiskohdat.";

$LANG_dbinstall_head="Tietokannan tiedot";
$LANG_varedit_dbhost="Tietokannan ip-osoite (tai localhost)";
$LANG_varedit_dblogin="Tietokannan kirjautumisnimi";
$LANG_varedit_dbpass="Tietokannan salasana";
$LANG_varedit_dbname="Tietokannan nimi";

$LANG_pathing_head="Polku & Admin tietoa";
$LANG_varedit_baseurl="Bannerinvaihdon perusosoite (URL)";
$LANG_varedit_baseurl_note="�l� laita vinoviivaa loppuun";
$LANG_varedit_basepath="Peruspolku";
$LANG_varedit_exchangename="Bannerinvaihdon nimi";
$LANG_varedit_sitename="Sivuston nimi";
$LANG_varedit_adminname="Adminin nimi";
$LANG_varedit_adminemail="Adminin s�hk�postiosoite";

$LANG_banners_head= "Bannerit";
$LANG_varedit_width="Bannerin leveys";
$LANG_varedit_height="Bannerin korkeus";
$LANG_varedit_pixels="pixeli�";
$LANG_varedit_defrat="Oletussuhde";
$LANG_varedit_showimage="N�yt� Bannerinvaihdon kuva";
$LANG_varedit_imageurl="Bannerivaihdon kuvan osoite (URL)";
$LANG_left="Vasen";
$LANG_right="Oikea";
$LANG_top="Yl�s";
$LANG_bottom="Alas";
$LANG_varedit_imageurl_msg="tarvitaan koko osoite";
$LANG_varedit_showtext="N�yt� bannerivaihdon linkki";
$LANG_varedit_exchangetext="Teksti";
$LANG_varedit_reqapproval="Vaadi bannereiden hyv�ksynt��";
$LANG_varedit_upload="Salli bannereiden l�hett�minen";
$LANG_varedit_maxsize="Suurin sallittu tiedostokoko";
$LANG_varedit_uploadpath="L�hetyspolku (ei vinoviivaa per��n)";
$LANG_varedit_upurl="L�hetyshakemiston osoite";
$LANG_varedit_maxbanners="Bannereiden suurin sallittu m��r�";

$LANG_anticheat="Anti-Huijaus";
$LANG_varedit_anticheat="Anti-Huijaus tapa";
$LANG_varedit_cookies="Ev�steet";
$LANG_varedit_db="Tietokanta";
$LANG_varedit_none="Ei mit��n";
$LANG_varedit_duration="Kesto";
$LANG_varedit_duration_msg="Sekuntia..";

$LANG_referral_credits="K�ytt�j�hankinta & Pisteet";
$LANG_varedit_referral="K�ytt�j�hankinta-ohjelma";
$LANG_varedit_bounty="K�ytt�j�hankinnasta saatavat pisteet";
$LANG_varedit_startcred="Aloituspisteet";
$LANG_varedit_sellcredits="Myy pisteit�";

$LANG_misc="Sekalaista";
$LANG_varedit_topnum="Top x n�ytet��n";
$LANG_varedit_topnum_other="Tilit";
$LANG_varedit_sendemail="L�het� Adminin s�hk�postia";
$LANG_varedit_usemd5="K�yt� MD5 salakoodattuja salasanoja";
$LANG_varedit_usegz="K�yt� gZip/Zend koodia";
$LANG_varedit_userand="K�yt� mySQL4 rand()";
$LANG_varedit_logclicks="Kirjaa klikkaukset lokiin";
$LANG_varedit_userandwarn="Vaatii mySQL 4 tai uudemman";
$LANG_date_format="P�iv�yksen muoto";

// Edit Style Sheet (/admin/editcss.php)
$LANG_editcss_directionstop="Valitse tyyli jota haluaisit k�ytt�� bannerivaihdossasi, sen j�lkeen klikkaa L�het�. Muutoksesi ilmestyv�t v�litt�m�sti. Jos et n�e tyyli� jonka haluaisit valita, l�het� se /templates/css hakemistoon bannerinvaihtosi juurihakemiston alla.";
$LANG_editcss_instructions1="Valitse tyyli jota haluat muokata alla olevasta listasta, sitten klikkaa L�het�. Tyylisi latautuu. Tee muutoksesi, sitten klikkaa tallenna. Sinun t�ytyy valita muokattu tyyli (jos et jo ole tehnyt niin) tallennettuasi jotta muutoksesi tulevat n�kyviin!";
$LANG_editcss_loadbutton="Lataa tyyli";

// FAQ Manager (/admin/faq.php)
$LANG_faq_found="UKK kysymys/kysymyksi� l�ydetty.";
$LANG_faq_add="Lis�� kohta";

// Check Banners (/admin/checkbanners.php)
$LANG_checkbanners_description="T�m� toiminto on suunniteltu tarkistamaan kaikki bannerit ja kohde-osoitteet bannerinvaihdossa t�ll� hetkell�. T�m� ty�kalu on hy�dyllinen puhdistettaessa toimimattomia tilej� ja katkenneita banneri linkkej� tietokannasta. Jos banneri tai kohde-osoite ei l�p�ise tarkistusta, tili otetaan automaattisesti pois kierrosta ja liitet��n varmennusjonoon. Voit sen j�lkeen uudelleen-varmentaa n�m� tilit.<p>Aloittaaksesi bannereiden tarkistuksen, klikkaa <b>Tarkista bannerit</b> linkki� alla. Huomaa ett� ei ole tarpeen k�ytt�� t�t� toimintoa jos bannerit ovat omalla serverill�si/sivustollasi.";
$LANG_status_OK="OK";
$LANG_status_broken="KATKENNUT";

// Edit COU/Rules (/admin/editstuff.php)
$LANG_editstuff_message="HTML on k�yt�ss�. Luo dokumennoinneista tiedot siit� mit� mikin asetus tekee (ne ovat melko itsest��nselvi�).<p> Kelpaavat asetukset:";
$LANG_editstuff_result="Sivua on muokattu.";
$LANG_editstuff_url="Klikkaa t�st�</a> n�hd�ksesi muutokset!";
$LANG_exchange_paused="<b>BANNERIVAIHTO ON KESKEYTETTY!</b> T�m� tarkoittaa ett� vain oletusbannerit n�ytet��n. K�ytt�j�t saavat edelleen pisteit� n�yt�ist�. Palataksesi normaaliin toimintaan, klikkaa <b>Jatka bannerivaihtoa</b> linkki�.";

// Promo/Coupon Manager (/admin/promos.php)
$LANG_promo_title="Tarjouksien hallinta";
$LANG_promo_noitems="T�ll� hetkell� ei ole toimivia tarjouksia. Voit luoda uusia tarjouksia k�ytt�m�ll� alla olevaa lomaketta.";
$LANG_promo_history="Katso";
$LANG_promo_name="Tarjouksen nimi";
$LANG_promo_code="Koodi";
$LANG_promo_type="Tyyppi";
$LANG_promo_credits="Pisteet";
$LANG_promo_status="Vaihtoehdot";
$LANG_promo_timestamp="Aloitusp�iv�m��r�";
$LANG_promo_type1="Massa pisteet"; 
$LANG_promo_type2="prosentin alennus"; // percentage off item. expressed as xx% off item.
$LANG_promo_type3="Erikoistarjous";
$LANG_promo_add="Lis�� tarjous";
$LANG_promo_value="Arvo (jos tarvitaan)";
$LANG_promo_reuse="K�ytt�j�t voivat k�ytt�� uudelleen kupongin koodia";
$LANG_promo_reuseint="Uudelleenk�yt�n v�liaika";
$LANG_promo_reusedays="Vuorokautta";
$LANG_promo_usertype="Kelpaava k�ytt�j�tyyppi";
$LANG_promo_newonly="Vain uudet k�ytt�j�t";
$LANG_promo_all="Kaikki k�ytt�j�t";
$LANG_promo_listall="Luetteloi kaikki";
$LANG_promo_listdel="Luetteloi poistetut";
$LANG_promo_listact="Luetteloi toimivat";
$LANG_promo_deleted="POISTETTU"; // Status of promo displayed in name field
$LANG_promo_active="K�YT�SS�";

// Promo Details.. (/admin/promodetails.php)
$LANG_promodet_title="Kuponki - Yksityiskohdat";
$LANG_promodet_overview="Kupongin yleiskuva";
$LANG_promodet_loghead="K�ytt�loki";
$LANG_promodet_nostats="T�t� kuponkia ei ole k�ytetty. Tilastot ovat saatavilla kun k�ytt�j� on kuponkia k�ytt�nyt.";
$LANG_promodet_id="Kupongin tunnus";
$LANG_promodet_name="Kupongin nimi";
$LANG_promodet_type="Kupongin tyyppi";
$LANG_promodet_code="Kupongin koodi";
$LANG_promodet_vals="Kupongin arvo";
$LANG_promodet_credits="Kupongin pisteet";
$LANG_promodet_reuse="K�ytt�j� voi k�ytt�� uudelleen";
$LANG_promodet_reuseint="uudelleenk�yt�n vanheneminen";
$LANG_promodet_reuseintdays="vuorokausia";
$LANG_promodet_usertype="K�ytt�j�tyyppi";
$LANG_promodet_timestamp="Luotu";
$LANG_promodet_status="Kupongin tila";
$LANG_promodet_usedate="K�ytetty p�iv�ys";

// Store Manager (/admin/commerce.php)
$LANG_commerce_title="Kaupan hallinta";
$LANG_commerce_noitems="Kaupassa ei ole t�ll� hetkell� tuotteita. Voit luoda uusia tuotteita k�ytt�en alla olevaa lomaketta.";
$LANG_commerce_name="Tuotteen nimi";
$LANG_commerce_credits="Pisteet";
$LANG_commerce_price="Hinta";
$LANG_commerce_purchased="Ostettu";
$LANG_commerce_options="Vaihtoehdot";
$LANG_commerce_add="Lis�� tuote";
$LANG_commerce_edititem="Muokkaa tuotetta";
$LANG_commerce_filterhead="Suodattimet";
$LANG_commerce_recordsperpage="Merkint�j� per sivu";

$LANG_commerce_view="Katso/Etsi tapahtumia";
$LANG_commerce_notrans="Ei yht��n tapahtumaa valittavana katsottavaksi. T�m� voi johtua joko siit� ett� tietokannassa ei ole yht��n tapahtumaa, tai ei ole tapahtumaa joka vastaisi suodattimia jotka olet valinnut.";
$LANG_next="Seuraava";
$LANG_previous="Edellinen";
$LANG_commerce_invoice="Lasku";
$LANG_commerce_user="K�ytt�j�";
$LANG_commerce_item="Tuote";
$LANG_commerce_status="Maksun tila";
$LANG_commerce_payment="Bruttomaksu";
$LANG_commerce_email="Maksajan s�hk�postiosoite";
$LANG_commerce_date="P�iv�ys";

$LANG_commerce_filterorders="N�yt� tilaukset ja tilanne";
$LANG_commerce_uidsearch="Etsi tilauksia k�ytt�j�nimen mukaan";
$LANG_commerce_go="Mene!";
$LANG_commerce_reset="Tyhjenn� suodattimet";

// Login Page (/admin/index.php)
$LANG_index_title="Adminin hallintapaneeliin kirjautuminen";
$LANG_index_msg="Bannerivaihto<br>Adminin hallintapaneeli";
$LANG_index_login="Kirjaudu";
$LANG_index_password="Salasana";

// Logout Page (/admin/logout.php)
$LANG_logout_title="Kirjautunut ulos";
$LANG_logout_msg="Olet onnistuneesti kirjautunut ulos!<p><a href=\"index.php\">Klikkaa T�st�</a> palataksesi kirjautumisn�ytt��n.";

// Update page (/admin/update.php)
$LANG_updatemgr_title="P�ivityksien hallinta";
$LANG_updatemgr_inst="P�ivityksien hallinta tarjoaa helpon tavan tarkistaa ja hankkia p�ivityksi� phpBannerExchange 2.x.een. Jotta p�ivityksien hallinta toimisi oikein, sinun t�ytyy muuttaa \"manifest.php\"  tiedoston oikeuksia(chmod 777). T�m� tiedosto sijaitsee bannerivaihdon juurihakemistossa. Me tarkistamme n�m� oikeudet osana p�ivityst�. Ole Hyv� ja valitse yksi seuraavista vaihtoehdoista:";
$LANG_updatemgr_full="T�ysi p�ivitys";
$LANG_updatemgr_fulldesc="P�ivitt�� manifest-tiedostosi nykyisill� versionumeroilla ja tarkistaa p�ivitykset p��serverilt�. (Suositeltava)";
$LANG_updatemgr_refresh="P�ivit� manifest";
$LANG_updatemgr_refreshdesc="P�ivitt�� vain manifest-tiedostosi nykyisill� versionumeroilla.";
$LANG_updatemgr_updonly="P�ivitys";
$LANG_updatemgr_updonlydesc="Hakee p�ivitykset ILMAN manifest-listan p�ivityst�. Jos manifest-listasi on j��nyt vanhaksi, ET saa uusimpia p�ivityksi�!";
$LANG_updatemgr_checkperms="Tarkistan <b>manifest.php</b> tiedoston oikeudet...";
$LANG_updatemgr_permok="Tiedosto <b>manifest.php</b> on kirjoitettavissa. Jatketaan...";
$LANG_updatemgr_manifestcheck="Tarkistan tiedostoversiot...";
$LANG_updatemgr_manifeststamp="Manifest on viimeksi p�ivitetty";
$LANG_updatemgr_never="Ei koskaan";
$LANG_updatemgr_url="P�ivit� osoite (URL)";
$LANG_updatemgr_popmanifest="T�ydenn�n Manifest-listaa. Ole Hyv� ja odota...";

// update manager errors (can't put these in the errors.php file)..
$LANG_updatemgr_permerror="<b>VIRHE!</b> Tiedosto <b>manifest.php</b> EI ole kirjoitettavissa! T�m� tarkoittaa ett� et voi p�ivitt�� manifest-listaa! Tarkista tiedoston oikeudet ja yrit� uudelleen (vihje: chmod 777 manifest.php).";
$LANG_updatemgr_nomanifest="<b>VIRHE!</b> Tiedostoa <b>manifest.php</b> ei l�ydy! Varmista ett� tiedosto manifest.php sijaitsee phpBannerExchange kansion juuressa ja yrit� uudelleen!";
$LANG_updatemgr_successwrite="Tiedosto <b>manifest.php</b> on kirjoitettu onnistuneesti! Jatketaan...";
$LANG_updatemgr_getmaster="Yrit�n avata yhteyden p��tiedostoon. T�m� voi vied� hetken.";

$LANG_updatemgr_cantconnect="Yhteys p�ivitysserverille ei onnistunut. T�h�n voi olla syyn� se ett� sinun PHP-versiosi ei tue t�t� ominaisuutta tai sinun serverisi admin on kytkenyt pois p��lt� \"<b>allow_url_fopen</b>\" ohjeistuksen. Se voi johtua my�s siit� ett� serveriin ei juuri nyt saa yhteytt�. Jos ongelma jatkuu, Ole Hyv� ja ota yhteys serverisi adminiin varmistaaksesi ett� k�yt�t PHP versiota 4.3.0 tai uudempaa ja <b>allow_url_fopen</b> ohjeistus on p��ll� PHP kokoonpanossasi.";
$LANG_updatemgr_compare="K�sittelen versiotietoa..ole hyv� ja odota..";
$LANG_updatemgr_uptodate="Sama versio";
$LANG_updatemgr_needsupdt="Eri versio";
// $number files found on the master list.
$LANG_updatemgr_valsfound="tiedostoa l�ydetty p��listalta.";
$LANG_updatemgr_notupgrade="Kaikki tiedostosi ovat ajan tasalla! Yht��n p�ivitetty� tiedostoa ei l�ydy ladattavaksi.";
$LANG_updatemgr_updwaiting="tiedosto(t) odottavat p�ivityst�. Seuraavat tiedostot on p�ivitetty ja ovat valmiina ladattavaksi:";
$LANG_updatemgr_updateinst="Asentaaksesi p�ivitykset, yksinkertaisesti tallennat tiedostot koneelle, uudelleennime�t .txt p��tteet .php p��tteiksi, ja l�het�t ne oikeisiin hakemistoihin serverillesi. Kansioiden nimet on annettu tiedostojen yhteydess� (esim: \"/index.php\" tarkoittaa ett� t�m� tiedosto on \"index.php\" tiedostosi joka kuuluu phpBannerExchange hakemiston juureen, \"/admin/validate.php\" on sinun \"validate.php\" tiedostosi joka kuuluu \"/admin\" kansioon, jne.";
$LANG_updatemgr_changelog="Katso p�ivityksen tiedot";

// Warnings..
$LANG_installdir_warning="VAROITUS! Ohjelma on havainnut ett� install-kansio on viel� serverill�! Y�m� on turvallisuusriski. On ehdottoman v�ltt�m�t�nt� ett� t�m� kansio poistetaan!";

// Generic words used all over the place
$LANG_yes="Kyll�";
$LANG_no="Ei";
$LANG_back="Takaisin";
$LANG_submit="L�het�";
$LANG_reset="Tyhjenn�";
$LANG_emailing="L�hett�� s�hk�postia";
$LANG_done="Valmis";
$LANG_ID="Tunnus";
$LANG_question="Kysymys";
$LANG_action="Toiminto";
$LANG_delete="Poista";
$LANG_edit="Muokkaa";
$LANG_answer="Vastaus";
$LANG_reactivate="Uudelleenk�ynnist�";

//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!

?>