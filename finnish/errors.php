<?
$file_rev="041305";
$file_lang="en";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Error title and headers..
$LANG_error="VIRHE";
$LANG_error_header="Seuraavat virheet havaittiin k�sky�si k�sitelt�ess�:";
$LANG_back="Takaisin";
$LANG_tryagain="Ole Hyv� ja <a href=\"javascript:history.go(-1)\">mene takaisin</a> ja yrit� uudelleen";

// Login error stuff
$LANG_login_error="Antamasi kirjautumistiedot ovat v��rin tai niit� ei l�ydy tietokannasta! Ole Hyv� ja <a href=\"index.php\">mene takaisin</a> ja yrit� uudelleen!";
// Client link is different..so we have to repeat this.
$LANG_login_error_client=$LANG_login_error="Tiedot jotka annoit ovat virheelliset tai niit� ei l�ydy tietokannasta! Ole Hyv� ja <a href=\"../index.php\">mene takaisin</a> ja yrit� uudelleen!";

// mysql connect error (used in both login and processing)
$LANG_error_mysqlconnect="Emme saaneet yhteytt� tietokantaan annetuilla tiedoilla. Tietokanta antoi seuraavan virhetuloksen:";

// ADMIN: add account error: We only check the username
// because we expect the admin to know what he/she is doing...
$LANG_addacct_error="K�ytt�j�nimi on jo k�yt�ss�! Ole Hyv� ja <a href=\"javascript:history.go(-1)\">mene takaisin</a> ja yrit� uudelleen";


// ADMIN: add admin errors..
$LANG_adminconf_login_long="Tiedot jotka annoit kirjautumisruudulla eiv�t kelpaa. (tulisi olla alle 20 merkki�)";
$LANG_adminconf_login_short="Tiedot jotka annoit kirjautumisruudulla eiv�t kelpaa. (tulisi olla ainakin 2 merkki�)";
$LANG_adminconf_login_inuse="Kirjautumistunnus $newlogin on jo k�yt�ss�";
$LANG_adminconf_pw_mismatch="Salasanasi eiv�t t�sm��";
$LANG_adminconf_pw_short="Tiedot jotka annoit salasanakent�ss� eiv�t kelpaa. (tulisi olla ainakin 4 merkki�)";
$LANG_adminconf_goback="Ole Hyv� ja <a href=\"javascript:history.go(-1)\">mene takaisin</a> ja yrit� uudelleen";
$LANG_adminconf_added="Tili on lis�tty";

// ADMIN: Category admin errors...
$LANG_addcat_tooshort="Kategorian nimen tulisi olla ainakin 2 mrkki� pitk�";
$LANG_addcat_toolong="Kategorian nimen ei tulisi olla enemp�� kuin 50 merkki�";
$LANG_addcat_exists="Kategorian nimi on jo k�yt�ss�";
$LANG_cats_nocats="Ei yht��n kategoriaa k�yt�ss�! Sinulla tulisi olla v�hint��n 1 kategoria!";
$LANG_delcat_default="Et voi poistaa oletuskategoriaa!";

// ADMIN/CLIENT: Change pw errors...
$LANG_pwconfirm_err_mismatch="Salasanasi eiv�t t�sm��!";
$LANG_pwconfirm_err_short="Tiedot jotka annoit salasanakent�ss� eiv�t kelpaa. (tulisi olla ainakin 4 merkki�).";
$LANG_pwconfirm_err_intro="Seuraavat virheet havaittiin k�sitelt�ess� k�sky�si";

// ADMIN/CLIENT: Upload/banner Errors...
$LANG_upload_blank="Kuvaan antamasi polku ei kelpaa. Ole Hyv� ja mene takaisin ja yrit� uudelleen";
$LANG_upload_not="Kuvasi l�hetys ei onnistunut. Ole Hyv� ja mene takaisin ja yrit� uudelleen";
$LANG_err_badimage="J�rjestelm� ei onnistunut l�yt�m��n banneriasi osoitteesta jonka annoit. T�m� johtuu siit� ett� se joko ei ole kuva, tai ei sijaitse osoitteessa jonka annoit (<b>$bannerurl</b>) jos olet l�hett�m�ss� t�t� kuvaae, tiedoston l�hett�misess� on voinut olla ongelma.  Ole Hyv� ja tarkista osoite ja yrit� uudelleen.  Huomioi ett� jos sivustosi on ilmaisilla kotisivun tarjoajilla kuten Geocities tai Angelfire, voi olla tarpeen liitt�� banneri jonnekin omalle sivullesi jotta sen linkitys sallitaan. Jotkin kotisivun tarjoajat eiv�t salli linkityst� lainkaan.";
$LANG_err_badwidth="Bannerisi ei kelpaa koska se on liian leve�. Bannereiden tulisi olla $bannerwidth pixeli� leveit�.";
$LANG_err_badheight="Bannerisi ei kelpaa koska se on liian korkea. Bannereiden tulisi olla $bannerheight pixeli� korkeita.";
$LANG_err_filesize="Bannerisi tiedostokoko ylitti suurimman sallitun koon. Bannerin ei tulisi olla yli $max_filesize bitti�.";

// Add admin error..
$LANG_adminconf_loginexist="Kirjautumistunnus on jo k�yt�ss�!";

// edit templates error..
$LANG_editcsstemplate_errornofile="Tiedostoa ei l�ydy! T�m� yleens� tarkoittaa ett� olet vaihtanut tiedoston nimen tai yritt�nyt ohittaa muuttujan v��r�ll� tiedostonimell�. Turvallisuussyist� johtuen, t�t� ei voi tehd�. Ole Hyv� ja mene takaisin ja yrit� uudelleen.";
$LANG_editcsstemplate_cannotwrite="phpBannerExchange ei pystynyt kirjoittamaan css.php tiedostoon. T�m� voi johtua v��rist� tiedoston oikeuksista (tulisi olla 755 tai 777) tai et p��se kirjoitamaan t�h�n tiedostoon. Tarkista tiedoston oikeudet ja p��syoikeudet ja yrit� uudelleen.";

// Promo Manager errors..
$LANG_promo_noproduct="Et antanut tuotteen nime�!";
$LANG_promo_badcode="Et antanut koodia, tai koodi on jo k�yt�ss�!";
$LANG_promo_noval="Sinun tulee antaa arvo \"value\" kent�ss� k�ytt��ksesi t�t� tarjoustyyppi�.";
$LANG_promo_nocreds="Sinun t�ytyy antaa arvo \"credits\" kent�ss� k�ytt��ksesi t�t� tarjoustyyppi�.";

// "COMMON"/PUBLIC SECTION ERRORS

// Lost Password error -- unable to locate account.
$LANG_lostpw_noacct="Emme onnistuneet l�yt�m��n tili� s�hk�postiosoitteelle <b>{email}</b>. Ole Hyv� ja yrit� uudelleen.";

// Signup Errors (/signupconfirm.php)
$LANG_err_nametooshort="Tiedot jotka annoit <b>Oikea nimi</b> kent�ss� eiv�t kelpaa. (tulisi olla enemm�n kuin 2 merkki�--Annoit <b>$_REQUEST[name]</b>)";
$LANG_err_nametoolong="Tiedot jotka annoit <b>Oikea nimi</b> kent�ss� eiv�t kelpaa. (tulisi olla v�hemm�n kuin 100 merkki�--Annoit <b>$_REQUEST[name]</b>)";
$LANG_err_loginshort="Tiedot jotka annoit <b>K�ytt�j�nimi</b> kent�ss� eiv�t kelpaa. (tulisi olla v�hemm�n kuin 20 merkki�--Annoit <b>$_REQUEST[login]</b>)";
$LANG_err_loginlong="Tiedot jotka annoit <b>K�ytt�j�nimi</b> kent�ss� eiv�t kelpaa. (tulisi olla v�hint��n 2 merkki�--Annoit <b>$_REQUEST[login]</b>)";
$LANG_err_logininuse="K�ytt�j�nimi $_REQUEST[login] on jo k�yt�ss�";
$LANG_err_emailinuse="<b>S�hk�postiosoite</b> jonka annoit, <b>$_REQUEST[email]</b>, on jo k�yt�ss�. Vain yksi tili per s�hk�postiosoite on sallittu";
$LANG_err_invalidurl="Sivuston osoite ei kelpaa. Ole Hyv� ja varmista ett� osoitteessasi on tiedostonimi (index.html, esimerkiksi) tai vinoviiva per�ss� (http://www.jokusivusto.com/)! Annoit <b>$_REQUEST[targeturl]</b>";
$LANG_err_badimage="J�rjestelm� ei pystynyt l�yt�m��n banneriasi osoitteesta jonka annoit. T�m� johtuu siit� ett� joko se ei ole kuva, tai se ei ole osoitteessa jonka annoit (<b>$_REQUEST[bannerurl]</b>).  Ole Hyv� ja tarkista osoite ja yrit� uudelleen.  Huomioi ett� jos sivustosi on ilmaisilla kotisivun tarjoajilla kuten Geocities tai Angelfire, voi olla tarpeen liitt�� banneri jonnekin omalle sivullesi jotta sen linkitys sallitaan. Jotkin kotisivun tarjoajat eiv�t salli linkityst� lainkaan.";
$LANG_err_badwidth="Bannerisi ei kelpaa koska se on liian leve�. Bannereiden tulisi olla $bannerwidth pixeli� leveit�.";
$LANG_err_badheight="Bannerisi ei kelpaa koska se on <b>$imageheight</b> pixeli� korkea. Bannereiden tulisi olla <b>$bannerheight</b> pixeli� korkeita.";
$LANG_err_email="J�rjestelm� ei voinut vahvistaa s�hk�postiosoitettasi koska siin� on erikoismerkkej�. Ole Hyv� ja ota yhteys adminiin ja pyyd� h�nt� avustamaan. (Annoit <b>$_REQUEST[email]</b>).";
$LANG_err_passmismatch="Salasanasi eiv�t t�sm��! <b>$_REQUEST[pass]</b> ei t�sm�� <b>$_REQUEST[pass2]</b> kanssa";
$LANG_err_passshort="Tiedot jotka annoit <b>Salasana</b> kent�ss� eiv�t kelpaa. (tulisi olla v�hint��n 4 merkki�-- Annoit <b>$_REQUEST[pass]</b>).";
$LANG_err_nocoupon="Kupongin koodi jonka annoit ei kelpaa tai se on v��r� kuponkityyppi! Kupongeissa t�ytyy olla IsOt Ja PiEnEt kirjaimet oikein!";

// Client coupon errors...
$LANG_coupon_wrongtype="T�m�n tyyppist� kuponkia ei voi k�ytt�� kaupassa!";
$LANG_coupon_nocoup="Kuponki jonka annoit ei kelpaa! Kupongeissa t�ytyy olla IsOt Ja PiEnEt kirjaimet oikein!";

// Promo code errors..
$LANG_coupon_clntwrongtype="T�m�n tyyppist� kuponkia voi k�ytt�� vain kaupassa!";
$LANG_coupon_noreuse="T�t� kuponkia ei voi k�ytt�� uudelleen!";
$LANG_coupon_userwrongtype="Et voi k�ytt�� t�t� kuponkia! T�t� kuponkia voi k�ytt�� vain rekister�ityess�!";
$LANG_coupon_noreuseyet="Et voi k�ytt�� t�t� kuponkia uudelleen t�ll� hetkell�! T�t� kuponkia voi k�ytt�� uudelleen $date_placeholder";

// Banner delete error
$LANG_bannerdel_error="Banneria ei voi poistaa koska se on jo poistettu vaihdosta! T�m� on voinut tapahtua monestakin syyst�. Mene takaisin bannereiden n�ytt�ruudulle ja varmista ett� banneri on viel� olemassa!";

// e-mail change error
 $LANG_infoconfirm_invalid="J�rjestelm� ei voinut vahvistaa s�hk�postiosoitettasi koska se sis�lt�� erikoismerkkej� tai se ei kelpaa. Ole Hyv� ja yrit� uudelleen tai ota yhteys adminiin ja pyyd� h�nt� avustamaan.";

 // Password change errors
$LANG_err_nopassmatch="Salasanasi eiv�t t�sm��!";
$LANG_err_passtooshort="Tiedot jotka annoit eiv�t kelpaa. (tulisi olla v�hint��n 4 merkki�.";
?>